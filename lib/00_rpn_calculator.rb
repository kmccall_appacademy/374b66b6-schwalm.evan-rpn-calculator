class RPNCalculator
  attr_accessor :calculator

  def initialize
    @stack = Array.new
  end

  def push(input)
    @stack << input
  end

  def plus
    operation(:+)
  end

  def minus
    operation(:-)
  end

  def times
    operation(:*)
  end

  def divide
    operation(:/)
  end

  def value
    @stack.last
  end

  def tokens(string)
    tokens = string.split
    tokens.map do |token|
      if %i(+ - * /).include?(token.to_sym)
        token.to_sym
      else
        Integer(token)
      end
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        operation(token)
      end
    end
    value
  end

  private

  def operation(symbol)
    raise "calculator is empty" if @stack.size < 2

    num2 = @stack.pop
    num1 = @stack.pop
    case symbol
    when :+
      @stack << num1 + num2
    when :-
      @stack << num1 - num2
    when :*
      @stack << num1 * num2
    when :/
      @stack << num1.fdiv(num2)
    else
      @stack << num1
      @stack << num2
      raise "invalid operation"
    end
  end

end
